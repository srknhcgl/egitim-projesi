CC=gcc

ROOT=../
INC_DIRS=-Iinclude -I$(ROOT)/include

results: src/main.c src/hellofunc.c include/hellomake.h
	$(CC) $(INC_DIRS) src/main.c src/hellofunc.c -o bin/hello.out

clean:
	rm -rf bin/*

run:
	./bin/hello.out